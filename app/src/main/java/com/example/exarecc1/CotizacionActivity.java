package com.example.exarecc1;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class CotizacionActivity extends AppCompatActivity {
    private TextView lblCliente;
    private TextView lblFolio;

    private EditText txtDescripcion;
    private EditText txtValorAuto;
    private EditText txtPorIn;

    private RadioButton rbtn12;
    private RadioButton rbtn18;
    private RadioButton rbtn24;
    private RadioButton rbtn36;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private TextView lblPagoMensual;
    private TextView lblEnganche;

    private Cotizacion cotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        iniciarComponentes();
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        lblCliente.setText(nombre);
        btnCalcular.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){ calcular();}
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){ limpiarCampos();}
        });btnRegresar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){ regresar();}
        });
    }

    private void iniciarComponentes() {

        lblFolio = findViewById(R.id.lblFolio);
        lblCliente = findViewById(R.id.lblCliente);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValorAuto);
        txtPorIn = findViewById(R.id.txtPorIn);
        rbtn12 = findViewById(R.id.rbtn12);
        rbtn12.setChecked(true);
        rbtn18 = findViewById(R.id.rbtn18);
        rbtn24 = findViewById(R.id.rbtn24);
        rbtn36 = findViewById(R.id.rbtn36);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblPagoMensual = findViewById(R.id.lblPagoMensual);
        lblEnganche = findViewById(R.id.lblEnganche);

        cotizacion = new Cotizacion(0,"",0,0,0);
        lblFolio.setText(""+cotizacion.generarFolio());
    }
    private void calcular() {
        String descripcion = txtDescripcion.getText().toString();
        String valorAutoStr = txtValorAuto.getText().toString();
        String pagoInicialStr = txtPorIn.getText().toString();

        if (descripcion.isEmpty()) {
            txtDescripcion.setError("Ingrese una descripción");
            return;
        }

        if (valorAutoStr.isEmpty()) {
            txtValorAuto.setError("Ingrese un valor válido");
            return;
        }

        if (pagoInicialStr.isEmpty()) {
            txtPorIn.setError("Ingrese un valor válido");
            return;
        }

        float valorAuto = Float.parseFloat(valorAutoStr);
        float pagoInicial = Float.parseFloat(pagoInicialStr);
        int plazo = 1;

        if (rbtn12.isChecked()) {
            plazo = 12;
        } else if (rbtn18.isChecked()) {
            plazo = 18;
        } else if (rbtn24.isChecked()){
            plazo = 24;
        } else if(rbtn36.isChecked()) {
            plazo = 36;
        }

        cotizacion.setDescripcion(descripcion);
        cotizacion.setValorAuto(valorAuto);
        cotizacion.setPorEnganche(pagoInicial);
        cotizacion.setPlazo(plazo);

        float enganche = cotizacion.CalcularEnganche(valorAuto, pagoInicial);
        float pagoMensual = cotizacion.CalcularPagoMensual(valorAuto, pagoInicial, plazo);

        lblPagoMensual.setText("Pago Mensual en: " + pagoMensual);
        lblEnganche.setText("Enganche: " + enganche);
    }

    private void limpiarCampos() {
        // Limpia los campos de texto y los resultados en los TextView
        txtValorAuto.setText("");
        txtPorIn.setText("");
        txtDescripcion.setText("");
        lblPagoMensual.setText("");
        lblEnganche.setText("");
    }
    private void regresar() {
        androidx.appcompat.app.AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Cotizacion APP");
        confirmar.setMessage("Regresar a la pantalla pricipal?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }
}