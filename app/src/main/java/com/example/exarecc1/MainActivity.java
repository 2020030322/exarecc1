package com.example.exarecc1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity  extends AppCompatActivity {
    private EditText txtClientes;
    private EditText txtCliente;
    private Button btnCotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });
    }

    private void iniciarComponentes() {
        txtClientes = findViewById(R.id.txtCliente);
        txtCliente = (EditText) findViewById(R.id.txtCliente);
        btnCotizacion = findViewById(R.id.btnCotizacion);
    }

    private void ingresar() {
        String textCliente = txtClientes.getText().toString().trim();
        if (textCliente.isEmpty()) {
            Toast.makeText(this, "Introduzca el nombre del cliente", Toast.LENGTH_SHORT).show();
        }else {

            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtCliente.getText().toString());
            bundle.putString("nombreCliente", txtClientes.getText().toString());

            Intent intent = new Intent(MainActivity.this, CotizacionActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}