package com.example.exarecc1;

public class Cotizacion {
    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazo;
    //Constructores
    Cotizacion(){
        // vacios
    }
    //Parametros
    Cotizacion(int folio, String descripcion, float valorAuto, float porEnganche, int plazo){
        this.folio = folio;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
        this.plazo = plazo;
    }
    //Por Copia
    Cotizacion(Cotizacion CA){
        this.folio = CA.folio;
        this.descripcion = CA.descripcion;
        this.valorAuto = CA.valorAuto;
        this.porEnganche = CA.porEnganche;
        this.plazo = CA.plazo;
    }
    //Setter & Getter
    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public int generarFolio(){
        return 0123;
    }

    public float CalcularEnganche(float valorAuto, float porEnganche){
        return valorAuto * (porEnganche / 100);
    }

    public float CalcularPagoMensual(float valorAuto, float porEnganche, int plazo){
        float enganche = CalcularEnganche(valorAuto, porEnganche);
        float totalFin = valorAuto - enganche;
        return totalFin/plazo;
    }
}
